# This should be a test or example startup script

require lakeshore336
epicsEnvSet("PREFIX", "IOC_LAKESHORE")
epicsEnvSet("IPADDR", "localhost")
epicsEnvSet("CALIB_FILES_PATH", "$(E3_CMD_TOP)/misc")

iocshLoad("$(lakeshore336_DIR)/lakeshore336.iocsh")
